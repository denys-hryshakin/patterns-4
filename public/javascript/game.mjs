import { createElement, addClass, removeClass } from "./helper.mjs";

const username = sessionStorage.getItem("username");

if (!username) {
  window.location.replace("/login");
}

const socket = io("", { query: { username } });

const roomsPage = document.getElementById("rooms-page");
const gamePage = document.getElementById("game-page");
const userCountElement = document.getElementById("user-counter");
const userList = document.getElementById("user-list");
const rightContainer = document.getElementById("right-container");
const commentContainer = document.getElementById("comment-container");
const commentPhraseElement = document.getElementById("comment-phrase");


const createAddButton = () => {
  const addRoomButton = createElement({
    tagName: "button", className: "btn ml-10", attributes: { id: "add-room-btn" }
  });

  addRoomButton.innerText = "Create Room";

  const onAddRoom = () => {
    const roomName = window.prompt("Enter the room name: ");
    if (roomName === '' || roomName === null) {
      return;
    }
    socket.emit("ADD_ROOM", ({ roomName, isGameStarted: false }));
    socket.on("rooms", ({ rooms }) => {
      createRoomButton(rooms.map(room => room.roomName));
    })
  };

  addRoomButton.addEventListener("click", onAddRoom);

  return addRoomButton;
};
roomsPage.append(createAddButton());

const createRoomButton = roomName => {
  const roomElement = createElement({
    tagName: "div", className: "rooms flex-centered no-select", attributes: { id: roomName }
  });
  const joinRoomButton = createElement({
    tagName: 'button', className: 'join-btn btn'
  });

  roomElement.innerText = roomName;
  joinRoomButton.innerText = "Join";

  const onJoinRoom = () => {
    socket.emit("JOIN_ROOM", roomName);
    socket.on('roomUsers', ({ users }) => {
      outputUsers(users);
    });
    createRoom(roomName);
  };

  joinRoomButton.addEventListener("click", onJoinRoom);

  roomElement.append(joinRoomButton);

  const outputUsers = (users) => {
    userList.innerHTML = '';
    users.forEach((user) => {
      const li = createElement({
        tagName: "li"
      });
      const status = createElement({
        tagName: "div", className: `ready-status-${user.status}`, attributes: { id: `ready-status-${user.username}` }
      });
      const progressBarContainer = createElement({
        tagName: "div", className: `user-progress-container`
      });
      const progressBar = createElement({
        tagName: "div", className: `user-progress ${user.username}`
      });

      li.innerText = user.username;

      progressBar.style.width = `${user.progress}%`;

      if (user.progress > 99) {
        addClass(progressBar, "finished");
      }

      progressBarContainer.appendChild(progressBar);
      li.append(status, progressBarContainer)

      userList.appendChild(li);
    });


  }

  const createRoom = roomName => {
    const gameContainer = createElement({
      tagName: "div", attributes: { id: "game-container" }
    });
    const roomNameElement = createElement({
      tagName: "h1", className: "display-block", attributes: { id: roomName }
    });
    const leaveRoomBtn = createElement({
      tagName: "button", className: "btn", attributes: { id: "quit-room-btn" }
    });
    const statusButton = createElement({
      tagName: "button", className: "btn", attributes: { id: "ready-btn" }
    });
    const timer = createElement({
      tagName: "div", className: "display-none", attributes: { id: "timer" }
    });
    const textContainer = createElement({
      tagName: "div", className: "display-none", attributes: { id: "text-container" }
    });

    const gameTimer = createElement({
      tagName: "div", className: "display-none", attributes: { id: "game-timer" }
    });
    const commentImage = createElement({
      tagName: "img", className: "", attributes: { id: "comment-img", src: "../images/commentator.png" }
    });
    const commentPhrase = createElement({
      tagName: "div", className: "", attributes: { id: "comment-phrase" }
    });

    roomNameElement.innerText = roomName;
    leaveRoomBtn.innerText = "Back Ro Rooms";
    statusButton.innerText = "READY";
    commentPhrase.innerText = "Вас приветствует комментатор клавогонок! Приятной игры!";

    const onLeaveRoom = () => {
      socket.emit("LEAVE_ROOM", roomName);
      gameContainer.remove();
      while (rightContainer?.firstChild) {
        rightContainer?.removeChild(rightContainer?.firstChild);
      }
      textContainer.remove();
      rightContainer.remove();
      statusButton.remove();
    }

    const onReady = () => {
      let status;
      switch (statusButton.innerText) {
        case "READY":
          statusButton.innerText = "NOT READY";
          status = "green";
          socket.emit("READY", ({ status }));
          break;
        case "NOT READY":
          statusButton.innerText = "READY";
          status = "red";
          socket.emit("READY", ({ status }));
          break;
        default:
          break;
      }
    }

    leaveRoomBtn.addEventListener("click", onLeaveRoom);
    statusButton.addEventListener("click", onReady);

    socket.on("START", ({ rdyStatus }) => {
      if (rdyStatus) {
        socket.on("MESSAGE", message => {
          commentPhrase.innerText = message;
        })
        addClass(statusButton, "display-none");
        removeClass(timer, "display-none");
        socket.on('timer', function (data) {
          timer.innerText = (data.countdown);
        });
        addClass(leaveRoomBtn, "display-none");

        setTimeout(() => {
          addClass(timer, "display-none");
          removeClass(textContainer, "display-none");
          removeClass(gameTimer, "display-none");

          socket.on('gameTimer', function (data) {
            gameTimer.innerText = `${data.countdown} seconds left`
          });

          let words = document.querySelectorAll(".word");
          function typing(e) {
            let typed = String.fromCharCode(e.which);
            for (var i = 0; i < words.length; i++) {
              if (words[i].innerHTML === typed) {
                if (words[i].classList.contains("bg")) {
                  continue;
                } else if (words[i].classList.contains("bg") === false && words[i - 1] === undefined || words[i - 1].classList.contains("bg") !== false) {
                  words[i].classList.add("bg");
                  let word = 100 / words.length;
                  socket.emit("PROGRESS", word);
                  break;
                }
              }
            }
          }
          document.addEventListener("keydown", typing, false);
          setTimeout(() => {
            document.removeEventListener("keydown", typing, false);
            removeClass(leaveRoomBtn, "display-none");
            socket.on("MESSAGE", message => {
              commentPhrase.innerText = message;
            })
          }, 61000);
        }, 11000);
      }
      socket.on("TEXT", ({ text }) => {
        let textArr = text.split("");
        textArr.forEach(item => {
          const p = createElement({
            tagName: "span", className: "word", attributes: { id: `${item} ` }
          });
          p.innerText = item;

          textContainer.append(p);
        });
      });
    })

    socket.on('roomUsers', ({ users }) => {
      if (users.find(user => user.progress >= 99)) {
        const winner = users.sort((a, b) => b.progress - a.progress).map(user => user.username);
        const winners = winner.join(', ');
        // alert(`Race Over! Results: ${winners}`);
        commentPhrase.innerText = `Race Over! Results: ${winners}`
      }
    });

    rightContainer.append(statusButton, timer, textContainer, gameTimer);
    commentContainer.append(commentImage, commentPhrase);
    gameContainer.append(userCountElement, roomNameElement, leaveRoomBtn, userList);
    gamePage.append(gameContainer, rightContainer, commentContainer);
  }

  return roomElement;
};

const updateRooms = rooms => {
  const allRooms = rooms.map(room => createRoomButton(room.roomName));

  const roomsContainer = createElement({
    tagName: "div",
    className: "rooms-container",
    attributes: { id: "room-container" }
  })

  roomsContainer.innerHTML = "";

  roomsContainer.append(...allRooms);
  roomsPage.append(roomsContainer);
};

const updateUserCount = userCounter => {
  userCountElement.innerText = `Users in the room: ${userCounter} `;
};

const joinRoomDone = ({ userCounter }) => {
  updateUserCount(userCounter);
  addClass(roomsPage, "display-none");
  addClass(gamePage, "display-flex");
  removeClass(gamePage, "display-none");
};

const leaveRoomDone = ({ userCounter }) => {
  updateUserCount(userCounter);
  removeClass(roomsPage, "display-none");
  removeClass(gamePage, "display-flex");
  addClass(gamePage, "display-none");
};


socket.on("UPDATE_ROOMS", updateRooms);
socket.on("JOIN_ROOM_DONE", joinRoomDone);
socket.on("LEAVE_ROOM_DONE", leaveRoomDone);
socket.on("UPDATE_USER_COUNTER", updateUserCount);

// ERROR HANDLERS
socket.on("ROOM_NAME_FAILED", () => {
  addClass(gamePage, "display-none");
  removeClass(roomsPage, "display-none");
  window.alert("Oops, room with name already exists :(");
});