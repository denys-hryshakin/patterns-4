import * as config from "./config";
import { texts, phrases } from "../data";
import { Users } from "./users";
import { Rooms, rooms } from "./rooms";

export default io => {
  io.on("connection", socket => {
    socket.emit("UPDATE_ROOMS", rooms);

    const username = socket.handshake.query.username;
    const userStatus = "red";
    const userProgress = 0;

    socket.on("ADD_ROOM", ({ roomName, isGameStarted }) => {
      const roomNameFailed = () => socket.emit('ROOM_NAME_FAILED');
      Rooms.addRoom(roomName, roomNameFailed, isGameStarted);
      io.emit("rooms", { rooms: Rooms.getRoom(roomName) });
    });

    socket.on("JOIN_ROOM", roomName => {
      socket.join(roomName);
      Users.userJoin(socket.id, username, roomName, userStatus, userProgress);
      const userCounter = Rooms.getNumberRoomUsers(io, roomName)

      // if (numClients <= config.MAXIMUM_USERS_FOR_ONE_ROOM) {
      io.to(socket.id).emit("JOIN_ROOM_DONE", ({ userCounter }));
      io.to(roomName).emit("UPDATE_USER_COUNTER", userCounter);
      // }

      io.to(roomName).emit('roomUsers', { users: Users.getRoomUsers(roomName) });
      console.log({ users: Users.getRoomUsers(roomName) })

      socket.on("READY", ({ status }) => {
        const user = Users.getCurrentUser(socket.id);
        user.status = status;
        io.to(roomName).emit("TEXT", ({ text: Rooms.getText() }))

        io.to(roomName).emit('roomUsers', { users: Users.getRoomUsers(roomName) });
        io.to(roomName).emit("START", { rdyStatus: Rooms.startGame(roomName) });
        if (Rooms.startGame(roomName)) {
          let message = "Игра вот-вот начнется! Всем удачи!"
          io.to(roomName).emit("MESSAGE", message);
          var beforeCountdown = config.SECONDS_TIMER_BEFORE_START_GAME;
          var gameCountdown = config.SECONDS_FOR_GAME;
          const timer = setInterval(() => {
            if (beforeCountdown <= 1) {
              clearInterval(timer);
            }
            beforeCountdown -= 1;
            io.to(roomName).emit("timer", { countdown: beforeCountdown });
          }, 1000);

          setTimeout(() => {
            const gameTimer = setInterval(() => {
              if (gameCountdown <= 1) {
                clearInterval(gameTimer);
              }
              gameCountdown -= 1;
              io.to(roomName).emit("gameTimer", { countdown: gameCountdown });
            }, 1000);
            setTimeout(() => {
              message = "Время вышло!"
              io.to(roomName).emit("MESSAGE", message);
            }, 61000);
            setTimeout(() => {
              message = "Шутка****"
              io.to(roomName).emit("MESSAGE", message);
            }, 25000);
          }, 10000);

        }
        socket.on("PROGRESS", progressChange => {
          user.progress = user.progress + progressChange;
          io.to(roomName).emit('roomUsers', { users: Users.getRoomUsers(roomName) });
        })
      });

    });

    socket.on("LEAVE_ROOM", roomName => {
      userLeave(socket.id);
      io.to(roomName).emit('roomUsers', { users: Users.getRoomUsers(roomName) });
      io.to(roomName).emit('rooms', { rooms: Rooms.getRoom(roomName) });
      let userCounter = Rooms.getNumberRoomUsers(io, roomName)
      userCounter--;

      io.to(socket.id).emit("LEAVE_ROOM_DONE", ({ userCounter }));
      io.to(roomName).emit("UPDATE_USER_COUNTER", userCounter);
      socket.leave(roomName);
    })

    socket.on("disconnect", roomName => {
      Users.userLeave(socket.id);

      socket.leave(roomName);
    })
  });
};
