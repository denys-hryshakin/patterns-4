import { texts, phrases } from "../data";
import { Users } from "./users";

export const rooms = [];

class Room {
    startGame(roomName) {
        const users = Users.getRoomUsers(roomName)
        const isReady = users.every(user => user.status === "green");

        return isReady;
    }

    getNumberRoomUsers(io, roomName) {
        const clients = io.sockets.adapter.rooms[roomName].sockets;
        const numClients = clients ? Object.keys(clients).length : 0;

        return numClients;
    }

    addRoom(roomName, roomNameFailed, isGameStarted) {
        const room = { roomName, isGameStarted };
        if (rooms.includes(roomName)) {
            roomNameFailed();
            return room;
        }
        rooms.push(room);

        return room;
    }

    getRoom(roomName) {
        return rooms.filter(room => (room.roomName === roomName && room.isGameStarted === false));
    }

    getText() {
        return texts[0];
    }
}

export const Rooms = new Room();
