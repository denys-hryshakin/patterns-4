export const users = [];

class User {

    userJoin(id, username, room, status, progress) {
        const user = { id, username, room, status, progress };
        if (users.includes(user)) {
            return users;
        }
        users.push(user);

        return user;
    }
    getCurrentUser(id) {
        return users.find(user => user.id === id);
    }

    userLeave(id) {
        const index = users.findIndex(user => user.id === id);

        if (index !== -1) {
            return users.splice(index, 1)[0];
        }
    }

    getRoomUsers(room) {
        return users.filter(user => user.room === room);
    }
}

export const Users = new User();
